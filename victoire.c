#include "gounki.h"
#include <string.h>
#include <stdlib.h>

int elim(piece** board, char playercol){
  int i, j;

  for (i = 0; i < 8; i++)
    for (j = 0; j < 8; j++)
      if (board[i][j].col == playercol)
	return -1;
  return 0;
}

int check_victoire(char *input){
  if (strlen(input) != 3)
    return -1;

  if (input[2] != '#')
    return -1;

  if (is_correct_col(input[0]) == -1 || is_correct_line(input[1]) == -1)
    return -1;
  return 0;
}

int victoire(piece** board, char* input, char playercol){

  piece* considere;
  char* test;

  if (check_victoire(input))
    return -1;

  considere = &(board[7 - input[1] + '1'][input[0] - 'a']);

  if (considere -> col != playercol)
    return -2;

  if ((input[1] == '8' && playercol == 'B') || (input[1] == '1' && playercol == 'N'))
    return 0;

  test = (char *)malloc(5 * sizeof(char));

  test[0] = input[0];
  test[1] = input[1];
  test[2] = '-';

  if (nbcarre(*considere) + nbrond(*considere) >= 2 &&
      ((input[1] == '7' && playercol == 'B') || (input[1] == '2' && playercol == 'N'))){
    
    if (playercol == 'B')
      test[4] = '8';
    else
      test[4] = '1';

    if (input[0] != 'a'){
      test[3] = input[0] - 1;
      if (board[7 - test[4] + '1'][test[3] - 'a'].col != '0')
	return -2;
      if (mouvement(board, test, playercol, 0) == 0){
	free(test);
	return 0;
      }
    }
    if (input[0] != 'h'){
      test[3] = input[0] + 1;
      if (board[7 - test[4] + '1'][test[3] - 'a'].col != '0')
	return -2;
      if (mouvement(board, test, playercol, 0) == 0){
	free(test);
	return 0;
      }
    }

    test[3] = input[0];
    if (board[7 - test[4] + '1'][test[3] - 'a'].col != '0')
      return -2;
    if (mouvement(board, test, playercol, 0) == 0){
      free(test);
      return 0;
    }
  }
  if (nbrond(*considere) == 2 && nbcarre(*considere) == 1 &&
      ((input[1] == '7' && playercol == 'B') || (input[1] == '2' && playercol == 'N'))){

    free(test);
    test = (char *)malloc(8 * sizeof(char));
    test[0] = input[0];
    test[1] = input[1];
    test[2] = '+';

    if (playercol == 'B')
      test[4] = '7';
    else
      test[4] = '2';

    test[5] = '-';
    if (playercol == 'B')
      test[7] = '8';
    else
      test[7] = '1';

    if (input[0] >= 'c'){
      test[3] = input[0] - 1;
      test[6] = test[3] - 1;
      if (deploiement(board, test, playercol, 0) == -3){
	free(test);
	return 0;
      }
    }
    if (input[0] <= 'f'){
      test[3] = input[0] + 1;
      test[6] = test[3] + 1;
      if (deploiement(board, test, playercol, 0) == -3){
	free(test);
	return 0;
      }
    }
  }

  if (nbrond(*considere) + nbcarre(*considere) == 3 &&
      ((input[1] == '6' && playercol == 'B') || (input[1] == '3' && playercol == 'N'))){

    free(test);
    test = (char *)malloc(8 * sizeof(char));
    test[0] = input[0];
    test[1] = input[1];

    if (playercol == 'B')
      test[4] = '7';
    else
      test[4] = '2';
    
    test[5] = '-';
    if (playercol == 'B')
      test[7] = '8';
    else
      test[7] = '1';

    /* traitement du cas d'abord vers le haut */
    if (nbcarre(*considere) >= 1){
      test[2] = '+';
      test[3] = test[0];
      
      

      if (nbcarre(*considere) == 1){
	if (input[0] != 'a'){
	  test[6] = input[0] - 1;
	  if (deploiement(board, test, playercol, 0) == -3){
	    free(test);
	    return 0;
	  }
	}
	if (input[0] != 'h'){
	  test[6] = input[0] + 1;
	  if (deploiement(board, test, playercol,0) == -3){
	    free(test);
	    return 0;
	  }
	}
      }
      if (nbcarre(*considere) >= 2){
	test[6] = input[0];
	if (deploiement(board, test, playercol, 0) == -3){
	  free(test);
	  return 0;
	}
      }
    }

    /* Ensuite diagonale */
    if (nbrond(*considere) >= 1){
      test[2] = '*';
      if (input[0] != 'a'){
	test[3] = input[0] - 1;
	if (nbrond(*considere) == 1){
	  test[6] = test[3];
	  if (deploiement(board, test, playercol, 0) == -3){
	    free(test);
	    return 0;
	  }
	}
	else{
	  if (test[3] != 'a'){
	    test[6] = test[3] - 1;
	    if (deploiement(board, test, playercol, 0) == -3){
	      free(test);
	      return 0;
	    }
	  }
	  else{
	    test[6] = test[3] + 1;
	    if (deploiement(board, test, playercol, 0) == -3){
	      free(test);
	      return 0;
	    }
	  }
	}
      }

      if (input[0] != 'h'){
	test[3] = input[0] + 1;
	if (nbrond(*considere) == 1){
	  test[6] = test[3];
	  if (deploiement(board, test, playercol, 0) == -3){
	    free(test);
	    return 0;
	  }
	}
	else{
	  if (test[3] != 'h'){
	    test[6] = test[3] + 1;
	    if (deploiement(board, test, playercol, 0) == -3){
	      free(test);
	      return 0;
	    }
	  }
	  else{
	    test[6] = test[3] - 1;
	    if (deploiement(board, test, playercol, 0) == -3){
	      free(test);
	      return 0;
	    }
	  }
	}
      }
    }
  }
  return -1;
}
