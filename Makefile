# Variables : 
# CC : compiler used
# CFLAGS : flags used for compilation

CC = gcc
CFLAGS = -Wall -Werror -pedantic -std=c89

SOURCES = main.c printing.c deploiement.c mouvement.c board.c victoire.c iarandom.c
OBJECTS = $(SOURCES:.c=.o)

all: gounki

# Explication : On a besoin de tout les objects pour faire les executables, donc on les genere.
# puis on utilise le compilateur pour les lier.

gounki: $(OBJECTS)
	$(CC) $(CFLAGS) $(OBJECTS) -o gounki

# Utiliser le compilateur pour compiler le .c en .o, $< faisant référence au .c et $@ au .o (target)
# -c : ne pas faire de liens à ce stade.

.c.o:
	$(CC) $(CFLAGS) $< -c -o $@

clean :
	rm *.o gounki