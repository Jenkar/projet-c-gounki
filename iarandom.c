#include "gounki.h"
#include <stdlib.h>
#include <time.h>

char* genrandom(piece** board, char botcolor){
  int i, j, k, l, m, n, o, p, q, r, resad;
  char *res;
  char **possible;
  int tmperror;

  possible = (char **)malloc(1000 * sizeof(char *));
  /*  Note : k = compteur pour se déplacer dans les possibles */
  k = 0;
  for (i = 0; i < 1000; i++)
    possible[i] = (char *)malloc(13 * sizeof(char));
  for (i = 0; i < 8; i++)
    for (j = 0; j < 8; j++)
      if (board[i][j].col == botcolor){	
	possible[k][0] = 'a' + j;
	possible[k][1] = '8' - i;

	/* test de victoire */
	possible[k][2] = '#';
	possible[k][3] = 0;
	if (victoire(board, possible[k], botcolor) == 0){
	  k++;
	  possible[k][0] = 'a' + j;
	  possible[k][1] = '8' - i;
	}

	/* test de mouvement */
	possible[k][2] = '-';
	
	possible[k][5] = 0;
	for (l = 0; l < 4; l++)
	  for (m = -3; m < 4; m++){
	    if (botcolor == 'B'){
	      possible[k][4] = possible[k][1] + l;
	      possible[k][3] = possible[k][0] + m;
	      if (mouvement(board, possible[k], botcolor, 0) == 0){
		k++;
		possible[k][0] = 'a' + j;
		possible[k][1] = '8' - i;
		possible[k][2] = '-';
		possible[k][5] = 0;
	      }
	    }
	    else{
	      possible[k][4] = possible[k][1] - l;
	      possible[k][3] = possible[k][0] + m;
	      if (mouvement(board, possible[k], botcolor, 0) == 0){
		k++;
		possible[k][0] = 'a' + j;
		possible[k][1] = '8' - i;
		possible[k][2] = '-';
		possible[k][5] = 0;
	      }
	    }
	  }


	/* tests de déploiement */
	for (l = 0; l < 2; l++)
	  for (m = -1; m < 2; m++){
	    if (l != 0 && m != 0)
	      possible[k][2] = '*';
	    else
	      possible[k][2] = '+';
	    possible[k][3] = possible[k][0] + m;
	    if (botcolor == 'B')
	      possible[k][4] = possible[k][1] + l;
	    else
	      possible[k][4] = possible[k][1] - l;
	    possible[k][5] = '-';
	    for (n = 0; n < 2; n++)
	      for (o = -1; o < 2; o++){

		possible[k][6] = possible[k][3] + o;
		if (botcolor == 'B')
		  possible[k][7] = possible[k][4] + n;
		else
		  possible[k][7] = possible[k][4] - n;
		possible[k][8] = 0;
		tmperror = deploiement(board, possible[k], botcolor, 0);

		if (tmperror == 0){
		  k++;
		  for (r = 0; r < 6; r++)
		    possible[k][r] = possible[k-1][r];
		}
		else if (tmperror == -3){
		  possible[k][8] = '-';
		  for (p = 0; p < 2; p++)
		    for (q = -1; q < 2; q++){
		      possible[k][9] = possible[k][6] + q;
		      if (botcolor == 'B')
			possible[k][10] = possible[k][7] + p;
		      else
			possible[k][10] = possible[k][7] - p;
		      possible[k][11] = 0;
		      
		      if (deploiement(board, possible[k], botcolor, 0) == 0){
			k++;
			for (r = 0; r < 9; r++)
			  possible[k][r] = possible[k-1][r];
		      }
		    }
		}
				
	      }
	  }
      }
  srand(time(NULL));
  resad = rand()%k;
  res = possible[resad];
  for (i = 0; i < k; i++){
    if (i != resad)
      free(possible[i]);
  }
  free(possible);
  return res;
}
	
