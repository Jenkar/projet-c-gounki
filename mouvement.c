#include "gounki.h"
#include <string.h>

int check_correct_syntax_move(char *input){
  if (strlen(input) != 5)
    return -1;
  if (is_correct_col(input[0]) == -1 || is_correct_col(input[3]) == -1 || \
     is_correct_line(input[1]) == -1 || is_correct_line(input[4]) == -1 || input[2] != '-')
    return -1;
  return 0;
}

/*
  Celle la aussi faudras couper
*/

int mouvement(piece **board, char *input, char playercol, char changeval){
  int cr1, cr2, cr3, col;
  int i;
  piece *ancien, *nouveau, *tmp;

  /* décortication : */

  if (check_correct_syntax_move(input) == -1) /* Verification de la syntaxe */
    return -1;

  ancien = &(board[7 - input[1] + '1'][input[0] - 'a']);
  nouveau = &(board[7 - input[4] + '1'][input[3] - 'a']);

  cr1 = ancien->cr1;
  cr2 = ancien->cr2;
  cr3 = ancien->cr3;
  col = ancien->col;

  if (col != playercol) /* Verification que l'on déplace bien son propre pion */
    return -2;

  if ((input[1] > input[4] && col != 'N') || (input[1] < input[4] && col != 'B')) /* Déplacement en arrière */
    return -2;

  if (input[3] - input[0] == 0 && input[4] - input[1] == 0){
    if (nbcarre(*ancien) < 2)
      return -2;
    if (input[0] == 'b')
      tmp = &(board[7 - input[1] + '1'][0]);
    else if (input[0] == 'g')
      tmp = &(board[7 - input[1] + '1'][7]);
    else
      return -2;
    if (tmp->col != '0')
      return -2;
    return 0;
  }


  else if (abs(input[3] - input[0]) == abs(input[4] - input[1])){ /* Mouvement rond classique */
    if (abs(input[3] - input[0]) > nbrond(*ancien))
      return -2;
    for (i = 1; i < abs(input[3] - input[0]); i++){
      if (input[3] - input[0] < 0){
	if (col == 'B')
	  tmp = &(board[7-input[1] + '1' - i][input[0] - 'a' - i]);

	else
	  tmp = &(board[7 - input[1] + '1' + i][input[0] - 'a' - i]);
      }
      else{
	if (col == 'B')
	  tmp = &(board[7-input[1] + '1' - i][input[0] - 'a' + i]);

	else
	  tmp = &(board[7 - input[1] + '1' + i][input[0] - 'a' + i]);
      }

      if (tmp->col != '0')
	return -2;

    }
  }

  else if (input[4] - input[1] == 0){
    if (abs(input[3] - input[0]) > nbcarre(*ancien))
      return -2;
    for (i = 1; i < abs(input[3] - input[0]); i++){
      if (input[3] - input[0] < 0)
	tmp = &(board[7 - input[1] + '1'][input[0] - 'a' - i]);
      else
	tmp = &(board[7 - input[1] + '1'][input[0] - 'a' + i]);

      if (tmp->col != '0')
	return -2;
    }
  }

  else if (input[3] - input[0] == 0){
    if (abs(input[4] - input[1]) > nbcarre(*ancien)){
      if (nbrond(*ancien) <= 1)
	return -2;
      if (input[0] + input[3] - 2 * 'a'> nbrond(*ancien) && 14 - input[0] - input[3] + 2 * 'a' > nbrond(*ancien))
	return -2;
      if (input[0] + input[3] - 2 * 'a' != abs(input[1] - input[4]) && 14 - input[0] - input[3] + 2*'a' != abs(input[1] - input[4]))
	return -2;
      if (input[0] == 'b'){
	if (col == 'B')
	  tmp = &(board[7 - input[1] + '1' - 1][0]);
	else
	  tmp = &(board[7 - input[1] + '1' + 1][0]);
      }

      else{
	if (col == 'B')
	  tmp = &(board[7 - input[1] + '1' - 1][7]);
	else
	  tmp = &(board[7 - input[1] + '1' + 1][7]);
      }
      if (tmp->col != '0')
	return -2;
    }

    else{
      for (i = 1; i < abs(input[4] - input[1]); i++){
	if (col == 'B')
	  tmp = &(board[7 - input[1] + '1' - i][input[0] - 'a']);
	else
	  tmp = &(board[7 - input[1] + '1' + i][input[0] - 'a']);
	if (tmp->col != '0')
	  return -2;
      }
    }
  }

  else{/* rebond */

    if (nbrond(*ancien) <= 1)
      return -2;

    if (input[0] + input[3] - 2 * 'a'> nbrond(*ancien) && 14 - input[0] - input[3] + 2 * 'a' > nbrond(*ancien))
      return -2;
    if (input[0] + input[3] - 2 * 'a' != abs(input[1] - input[4]) && 14 - input[0] - input[3] + 2*'a' != abs(input[1] - input[4]))
      return -2;

    if (input[0] == 'b'){
      if (col == 'B')
	tmp = &(board[7 - input[1] + '1' - 1][0]);
      else
	tmp = &(board[7 - input[1] + '1' + 1][0]);
    }

    else if (input[0] == 'g'){
      if (col == 'B')
	tmp = &(board[7 - input[1] + '1' - 1][7]);
      else
	tmp = &(board[7 - input[1] + '1' + 1][7]);
    }

    else if (input[0] == 'c'){
      if (col == 'B')
	tmp = &(board[7 - input[1] + '1' - 1][1]);
      else
	tmp = &(board[7 - input[1] + '1' + 1][1]);
    }

    else if (input[0] == 'f'){
      if (col == 'B')
	tmp = &(board[7 - input[1] + '1' - 1][6]);
      else
	tmp = &(board[7 - input[1] + '1' + 1][6]);
    }
    else
      return -2;

    if (tmp->col != '0')
      return -2;

    /* deuxième case */

    if (input[0] == 'b'){
      if (col == 'B')
	tmp = &(board[7 - input[1] + '1' - 2][1]);
      else
	tmp = &(board[7 - input[1] + '1' + 2][1]);
    }

    else if (input[0] == 'g'){
      if (col == 'B')
	tmp = &(board[7 - input[1] + '1' - 2][6]);
      else
	tmp = &(board[7 - input[1] + '1' + 2][6]);
    }

    else if (input[0] == 'c'){
      if (col == 'B')
	tmp = &(board[7 - input[1] + '1' - 2][0]);
      else
	tmp = &(board[7 - input[1] + '1' + 2][0]);
    }

    else if (input[0] == 'f'){
      if (col == 'B')
	tmp = &(board[7 - input[1] + '1' - 2][7]);
      else
	tmp = &(board[7 - input[1] + '1' + 2][7]);
    }

    if (tmp->col != '0')
      return -2;
  }

  if (nouveau->col == '0' || (nouveau->col == 'B' && col == 'N') ||\
     (nouveau->col == 'N' && col == 'B')){ /* On déplace un pion sur un emplacement vide OU sur un emplacement occupé par un adversaire */
    if (changeval == 0)
      return 0;
    nouveau->col = col;
    nouveau->cr1 = cr1;
    nouveau->cr2 = cr2;
    nouveau->cr3 = cr3;
  }
  else if (nouveau->cr2 == 0 && cr3 == 0){ /* On déplace un pion sur un de ses propres pions (composition) qui est de taille un */
    if (changeval == 0)
      return 0;
    nouveau->cr2 = cr1;
    nouveau->cr3 = cr2;
  }
  else if (nouveau->cr3 == 0 && cr3 == 0 && cr2 == 0){ /* Même chose, mais de taille deux */
    if (changeval == 0)
      return 0;
    nouveau->cr3 = cr1;
  }
  else /* Erreur syntaxique */
    return -2;

  /* On met l'emplacement ancien à 0 */
  ancien->cr1 = ancien->cr2 = ancien->cr3 = 0;
  ancien->col = '0';
  return 0;
}
