#include "gounki.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

int check_deploiement(char *input){
  int l = strlen(input);

  if (l != 8 && l != 11)
    return -1;

  if (is_correct_col(input[0]) == -1 || is_correct_col(input[3]) == -1 || is_correct_col(input[6]) == -1 ||
      is_correct_line(input[1]) == -1 || is_correct_line(input[4])  == -1 || is_correct_line(input[7]) == -1 ||
      (input[2] != '+' && input[2] != '*') == -1 || input[5] != '-')
    return -1;

  if (l == 11)
    if (is_correct_col(input[9]) == -1 || is_correct_line(input[10]) == -1 ||
	input[8] != '-')
      return -1;
  return 0;
}

char nextdir(char currentx, char currenty, char nextx, char nexty){
  if (currenty < nexty){
    if (currentx < nextx)
      return '9'; /* Droite vers le haut */
    if (currentx == nextx)
      return '8'; /* Haut */
    return '7'; /* Gauche vers le haut */
  }

  if (currenty == nexty){
    if (currentx < nextx){
      if (currentx == 'g')
	return 'h'; /* Horizontal, avec rebond */
      return '6';
    }
    if (currentx == 'b')
      return 'h';
    return '4';
  }

  if (currentx < nextx)
    return '3';
  if (currentx == nextx)
    return '2';
  return '1';
}

/*
   Vas VRAIMENT falloir découper c'machin, c'est assez immonde.
*/

int deploiement(piece **board, char *input, char playercol, char changeval){
  char currentdir, prevdir;
  piece *ancien, *tmp, *stack;
  int visitDeposit[3];

  ancien = &(board[7 - input[1] + '1'][input[0] - 'a']);

  if (check_deploiement(input))
    return -1;

  if (ancien->col != playercol)
    return -2;

  if (nbcarre(*ancien) + nbrond(*ancien) < 2)
    return -2;

  currentdir = nextdir(input[0], input[1], input[3], input[4]);

  if (currentdir > '6' && currentdir != 'h' && playercol != 'B')
    return -2;
  if (currentdir < '4' && currentdir != 'h' && playercol != 'N')
    return -2;

  tmp = &(board[7 - input[4] + '1'][input[3] - 'a']);

  if (tmp -> col != playercol && tmp -> col != '0')
    return -2;

  if (tmp -> cr3 != 0)
    return -2;
  
  stack = (piece *)malloc(sizeof(piece));
  copie(stack, ancien);

  if (input[2] == '+'){
    if (nbcarre(*stack) == 0){
      free(stack);
      return -2;
    }
    if (!(abs(input[3] - input[0]) == 1 && input[4] - input[1] == 0) &&
	!(abs(input[4] - input[1]) == 1 && input[3] - input[0] == 0)){
      free(stack);
      return -2;
    }
    visitDeposit[0] = 2;
    removeCarre(stack);
  }
  else{
    if (nbrond(*stack) == 0){
      free(stack);
      return -2;
    }
    if (!(abs(input[3] - input[0]) == 1 && abs(input[4] - input[1]) == 1)){
      free(stack);
      return -2;
    }
    visitDeposit[0] = 1;
    removeRond(stack);
  }

  /* Visite de la seconde case */

  if (input[6] != input[0] || input[7] != input[1]){
    tmp = &(board[7 - input[7] + '1'][input[6] - 'a']);

    if ((tmp -> col != playercol && tmp -> col != '0') || tmp -> cr3 != 0){
      free(stack);
      return -2;
    }
  }

  prevdir = currentdir;
  currentdir = nextdir(input[3], input[4], input[6], input[7]);

  if (currentdir > '6' && currentdir != 'h' && playercol != 'B'){
    free(stack);
    return -2;
  }
  if (currentdir < '4' && currentdir != 'h' && playercol != 'N'){
    free(stack);
    return -2;
  }

  if (input[2] == '+'){
    if (nbcarre(*stack) != 0){
      /* Verification que l'on conserve la bonne direction */
      if (currentdir != prevdir && (prevdir != 'h' || (currentdir != '4' && currentdir != '6')) &&
	  (currentdir != 'h' || (prevdir != '4' && prevdir != '6'))){
	free(stack);
	return -2;
      }

      if (!(abs(input[6] - input[3]) == 1 && input[7] - input[4] == 0) &&
	  !(abs(input[7] - input[4]) == 1 && input[6] - input[3] == 0)){
	free(stack);
	return -2;
      }

      visitDeposit[1] = 2;
      removeCarre(stack);
    }
    else{
      if (abs(input[6] - input[3]) != 1 || abs(input[7] - input[4]) != 1){
	free(stack);
	return -2;
      }
      visitDeposit[1] = 1;
      removeRond(stack);
    }
  }
  else{
    if (nbrond(*stack) != 0){
      if (currentdir != prevdir && input[3] != 'a' && input[3] != 'h'){
	free(stack);
	return -2;
      }
      if (abs(input[6] - input[3]) != 1 || abs(input[7] - input[4]) != 1){
	free(stack);
	return -2;
      }

      visitDeposit[1] = 1;
      removeRond(stack);
    }
    else{
      if (!(abs(input[6] - input[3]) == 1 && input[7] - input[4] == 0) &&
	  !(abs(input[7] - input[4]) == 1 && input[6] - input[3] == 0)){
	free(stack);
	return -2;
      }

      visitDeposit[1] = 2;
      removeCarre(stack);
    }
  }

  if (strlen(input) == 11){

    if (nbcarre(*stack) + nbrond(*stack) != 1){
      free(stack);
      return -2;
    }
    if (nbcarre(*stack) == 1)
      stack -> cr1 = 2;
    else
      stack -> cr1 = 1;

    prevdir = currentdir;
    currentdir = nextdir(input[6], input[7], input[9], input[10]);

    tmp = &(board[7 - input[10] + '1'][input[9] - 'a']);
    if (tmp -> col != playercol && tmp -> col != '0'){
      free(stack);
      return -2;
    }
    if (tmp -> col == playercol && tmp -> cr3 != 0){
      free(stack);
      return -2;
    }
    if (input[10] == input[4] && input[9] == input[3] && tmp -> cr2 != 0){
      free(stack);
      return -2;
    }

    if (currentdir > '6' && currentdir != 'h' && playercol != 'B'){
      free(stack);
      return -2;
    }
    if (currentdir < '4' && currentdir != 'h' && playercol != 'N'){
      free(stack);
      return -2;
    }

    if (stack -> cr1 == visitDeposit[1]){
      if (stack -> cr1 == 1){
	if (currentdir != prevdir && input[6] != 'a' && input[6] != 'h'){
	  free(stack);
	  return -2;
	}
      }
      else
	if (currentdir != prevdir && (prevdir != 'h' || (currentdir != '4' && currentdir != '6')) &&
	    (currentdir != 'h' || (prevdir != '4' && prevdir != '6'))){
	  free(stack);
	  return -2;
	}
    }
    if (stack -> cr1 == 2){
      if (!(abs(input[10] - input[7]) == 1 && input[9] - input[6] == 0) &&
	  !(abs(input[9] - input[6]) == 1 && input[10] - input[7] == 0)){
	free(stack);
	return -2;
      }
      visitDeposit[2] = 2;
    }
    else{
      if (abs(input[9] - input[6]) != 1 || abs(input[10] - input[7]) != 1){
	free(stack);
	return -2;
      }
      visitDeposit[2] = 1;
    }
  }
  else if (nbcarre(*stack) + nbrond(*stack) != 0){
    free(stack);
    return -3;
  } /* indicateur pour victoire, on a pas besoin d'un déploiement complet */

  /* A priori tout s'est bien passé, on peut déposer les machins */
  free(stack);
  if (changeval == 0)
    return 0;
  ancien -> col = '0';
  ancien -> cr1 = 0;
  ancien -> cr2 = 0;
  ancien -> cr3 = 0;

  board[7 - input[4] + '1'][input[3] - 'a'].col = playercol;
  board[7 - input[7] + '1'][input[6] - 'a'].col = playercol;

  if (visitDeposit[0] == 1)
    addRond(&(board[7 - input[4] + '1'][input[3] - 'a']));
  else
    addCarre(&(board[7 - input[4] + '1'][input[3] - 'a']));

  if (visitDeposit[1] == 1)
    addRond(&(board[7 - input[7] + '1'][input[6] - 'a']));
  else
    addCarre(&(board[7 - input[7] + '1'][input[6] - 'a']));

  if (strlen(input) == 11){
    board[7 - input[10] + '1'][input[9] - 'a'].col = playercol;
    if (visitDeposit[2] == 1)
      addRond(&(board[7 - input[10] + '1'][input[9] - 'a']));
    else
      addCarre(&(board[7 - input[10] + '1'][input[9] - 'a']));
  }
  return 0;

}
