#include "gounki.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*

  Ce fichier contient toute les fonctionnalitées io (pour l'instant)
  ainsi que les parties ayant attrait a la partie la plus superficielle du programme

*/

int posin(char** argv,int argc, char* cmp){
  int i;
  
  for (i = 1; i < argc ; i++){
    if (strcmp(argv[i], cmp) == 0)
      return i;
  }
  return -1;
}     

void treat(char *tmp){
  int i;

  for (i = 0; i < strlen(tmp); i++)
    if (tmp[i] == '\n'){
      tmp[i] = 0;
      return;
    }
}

int test(){
  piece** tab = initialise();
  char* tmp;
  int i;
  int errcode;

  printtab(tab);
  i = 0;
  tmp = (char *)malloc(13 * sizeof(char));

  while(1){
    fgets(tmp, 13, stdin);
    treat(tmp);

    if (strlen(tmp) < 3)
      return -1;
    if (i % 2 == 0){
      if (tmp[2] == '-')
	errcode = mouvement(tab, tmp, 'B', 1);
      else if (strlen(tmp) == 3){
	errcode = victoire(tab, tmp, 'B');
	if (errcode == 0){
	  printf("Victoire : Coup numéro : %d\n", i + 1);
	  freeall(tab);
	  free(tmp);
	  return 0;
	}
      }
      else
	errcode = deploiement(tab, tmp, 'B', 1);
      if (errcode == 0 && elim(tab, 'N') == 0){
	printf("Victoire : Coup numéro : %d\n", i + 1);
	freeall(tab);
	free(tmp);
	return 0;
      }
    }
    else{
      if (tmp[2] == '-')
	errcode = mouvement(tab, tmp, 'N', 1);
      else if (strlen(tmp) == 3){
	errcode = victoire(tab, tmp, 'N');
	if (errcode == 0){
	  printf("Victoire : Coup numéro : %d\n", i + 1);
	  freeall(tab);
	  free(tmp);
	  return 0;
	}
      }
      else
	errcode = deploiement(tab, tmp, 'N', 1);
      if (errcode == 0 && elim(tab, 'B') == 0){
	printf("Victoire : Coup numéro : %d\n", i + 1);
	freeall(tab);
	free(tmp);
	return 0;
      }
    }
    if (errcode != 0){
      printf("Erreur au coup %d\n", (i+1));
      freeall(tab);
      free(tmp);
      return errcode;
    }
    printtab(tab);
    i++;
    printf("Coup numéro : %d\n", i);
  }
  freeall(tab);
  free(tmp);
}

void playrobvsrob(piece **tab, int i){
  char* tmp;
  char botcolor;
  if (tab == NULL)
    tab = initialise();
  printtab(tab);
  tmp = (char *)malloc(13 * sizeof(char));

  while(1){
    botcolor = (i%2 == 0) ? 'B' : 'N';
    tmp = genrandom(tab, botcolor); 
    printf("%s\n",tmp);
    
    if (strlen(tmp) < 3){
      printf("Veuillez entrer un mouvement correct..\n");
      continue;
    }

    if (i % 2 == 0){
      if (tmp[2] == '-'){
	if (mouvement(tab, tmp, 'B', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      else if (strlen(tmp) == 3){
	if (victoire(tab, tmp, 'B') != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
	break;
      }
      else{
	if (deploiement(tab, tmp, 'B', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      if (elim(tab, 'B') == 0)
	break;
    }
    else{
      if (tmp[2] == '-'){
	if (mouvement(tab, tmp, 'N', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      else if (strlen(tmp) == 3){
	if (victoire(tab, tmp, 'N') != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
	break;
      }
      else{
	if (deploiement(tab, tmp, 'N', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      if (elim(tab, 'B') == 0)
	break;
    }
    printtab(tab);
    i++;
  }
  free(tmp);
  printf("Bravo pour la victoire, ");
  if (i % 2 == 0)
    printf("robot Blanc\n");
  else
    printf("robot Noir\n");
}

void playhumanvsrob(piece **tab, char playercol, int i){
  char* tmp;
  char botcolor;
  if (tab == NULL)
    tab = initialise();
  printtab(tab);
  tmp = (char *)malloc(13 * sizeof(char));

  botcolor = (playercol == 'B') ? 'N' : 'B';
  while(1){
    if (i % 2 == 0 && playercol == 'B'){
      printf("Veuillez entrer votre mouvement, joueur Blanc : ");
      fgets(tmp, 13, stdin);
      treat(tmp);
    }
    else if (i%2 == 1 && playercol == 'N'){
      printf("Veuillez entrer votre mouvement, joueur Noir : ");
      fgets(tmp, 13, stdin);
      treat(tmp);
    }
    else{
      printf("Le robot joue..\n");
      tmp = genrandom(tab, botcolor); 
      printf("%s\n",tmp);
    }
    if (strlen(tmp) < 3){
      printf("Veuillez entrer un mouvement correct..\n");
      continue;
    }
    if (strcmp(tmp, "sauvegarder") == 0){
      printf("Veuillez entrer ou sauvegarder la partie : ");
      fgets(tmp, 100, stdin);
      treat(tmp);
      ficwrite(tab, tmp, i % 2);
      printf("La partie a été sauvegardé dans le fichier : %s\n", tmp);
      return;
    }
    if (i % 2 == 0){
      if (tmp[2] == '-'){
	if (mouvement(tab, tmp, 'B', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      else if (strlen(tmp) == 3){
	if (victoire(tab, tmp, 'B') != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
	break;
      }
      else{
	if (deploiement(tab, tmp, 'B', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      if (elim(tab, 'B') == 0)
	break;
    }
    else{
      if (tmp[2] == '-'){
	if (mouvement(tab, tmp, 'N', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      else if (strlen(tmp) == 3){
	if (victoire(tab, tmp, 'N') != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
	break;
      }
      else{
	if (deploiement(tab, tmp, 'N', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      if (elim(tab, 'B') == 0)
	break;
    }
    printtab(tab);
    i++;
  }
  free(tmp);
  printf("Bravo pour la victoire, ");
  if (i % 2 == 0){
    if (playercol == 'B')
      printf("joueur Blanc\n");
    else
      printf("robot Blanc\n");
  }
  else{
    if (playercol == 'B')
      printf("robot Noir\n");
    else
      printf("joueur Noir\n");
  }
}

void playhuman(piece **tab, int i){
  char* tmp;

  if (tab == NULL)
    tab = initialise();
  printtab(tab);
  tmp = (char *)malloc(13 * sizeof(char));

  while(1){
    printf("Veuillez entrer votre mouvement, joueur ");
    if (i % 2 == 0)
      printf("Blanc : ");
    else
      printf("Noir : ");
    fgets(tmp, 13, stdin);
    treat(tmp);
    
    if (strcmp(tmp, "sauvegarder") == 0){
      printf("Veuillez entrer ou sauvegarder la partie : ");
      fgets(tmp, 100, stdin);
      treat(tmp);
      ficwrite(tab, tmp, i % 2);
      printf("La partie a été sauvegardé dans le fichier : %s\n", tmp);
      return;
    }

    if (strlen(tmp) < 3){
      printf("Veuillez entrer un mouvement correct..\n");
      continue;
    }

    if (i % 2 == 0){
      if (tmp[2] == '-'){
	if (mouvement(tab, tmp, 'B', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      else if (strlen(tmp) == 3){
	if (victoire(tab, tmp, 'B') != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
	break;
      }
      else{
	if (deploiement(tab, tmp, 'B', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      if (elim(tab, 'B') == 0)
	break;
    }
    else{
      if (tmp[2] == '-'){
	if (mouvement(tab, tmp, 'N', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      else if (strlen(tmp) == 3){
	if (victoire(tab, tmp, 'N') != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
	break;
      }
      else{
	if (deploiement(tab, tmp, 'N', 1) != 0){
	  printf("Veuillez entrer un mouvement correct..\n");
	  continue;
	}
      }
      if (elim(tab, 'B') == 0)
	break;
    }
    printtab(tab);
    i++;
  }
  free(tmp);
  printf("Bravo pour la victoire, joueur ");
  if (i % 2 == 0)
    printf("Blanc\n");
  else
    printf("Noir\n");
}

int main(int argc, char **argv){
  piece **tab;
  int posconfig, posB, posN, post, i;

  tab = NULL;
  posconfig = posin(argv, argc, "-c");
  posB = posin(argv, argc, "-B");
  posN = posin(argv, argc, "-N");
  post = posin(argv, argc, "-t");
  i = 0;
  if (posconfig != -1){
    if (argc - 1 < posconfig){
      printf("Erreur, pas de fichier de configuration\n");
      return -1;
    }
    tab = ficinitialise(argv[posconfig +1], &i);
    if (tab == NULL){
      printf("Le fichier de configuration %s n'as pas pu etre chargé.\n", argv[posconfig +1]);
      return -1;
    }
  }
  if (post != -1)
    return test();
  else{
    if (posB >= argc){
      printf("Erreur, pas d'argument pour l'option -B\n");
      return -1;
    }
    if (posN >= argc){
      printf("Erreur, pas d'argument pour l'option -N\n");
      return -1;
    }
    if (posB != -1){
      if (strcmp(argv[posB +1], "humain") == 0){
	if (posN != -1){
	  if (strcmp(argv[posN +1], "humain") == 0)
	    playhuman(tab, i);
	  else if (strcmp(argv[posN +1], "robot") == 0)
	    playhumanvsrob(tab, 'B', i);
	  else{
	    printf("Erreur, mauvais argument pour l'option -N : %s\n", argv[posN +1]);
	    return -1;
	  }
	}
	else
	  playhuman(tab, i);
      }
      else if (strcmp(argv[posB +1], "robot") == 0){
	if (posN != -1){
	  if (strcmp(argv[posN +1], "humain") == 0)
	    playhumanvsrob(tab, 'N', i);
	  else if (strcmp(argv[posN +1], "robot") == 0)
	    playrobvsrob(tab, i);
	  else{
	    printf("Erreur, mauvais argument pour l'option -N : %s\n", argv[posN +1]);
	    return -1;
	  }
	}
	else
	  playhumanvsrob(tab, 'N', i);
      }
      else{
	printf("Erreur, mauvais argument pour l'option -B : %s\n", argv[posB +1]);
	return -1;
      }
    }
    else if (posN != -1){
      if (strcmp(argv[posN +1], "humain") == 0)
	playhuman(tab, i);
      else if (strcmp(argv[posN +1], "robot") == 0)
	playhumanvsrob(tab, 'B', i);
      else{
	printf("Erreur, mauvais argument pour l'option -N : %s\n", argv[posN +1]);
	return -1;
      }
    }
    else
      playhuman(tab, i);
  }
  return 0;
}

