#include "gounki.h"
#include <stdio.h>

/*
  Codage plus joli des charactères
*/

char charof(int t){
  if (t == 1)
    return 'o';
  if (t == 2)
    return 'x';
  return '0';
}

void printtab(piece** tab){
  int i;
  int j;
  printf("  a  b  c  d  e  f  g  h\n\n");
  for (i = 0; i < 8; i++){
    printf("%d ", 8 - i);
    for (j = 0; j < 8; j++)
      printf("%c%c ", tab[i][j].col, charof(tab[i][j].cr1));
    printf("%d\n", 8 - i);
    printf("  ");
    for (j = 0; j < 8; j++)
      printf("%c%c ", charof(tab[i][j].cr2), charof(tab[i][j].cr3));
    printf("\n\n");
  }
  printf("  a  b  c  d  e  f  g  h\n");
}
