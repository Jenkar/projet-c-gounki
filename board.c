#include "gounki.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*
  Opérations de base sur les pieces
*/


/*
  0 = vide
  1 = cercle
  2 = carré
  B = Blanc
  N = Noir
  0 = Vide
 */

int abs(int x){
  if (x < 0)
    return -x;
  return x;
}

void freeall(piece** board){
  int i;

  for (i = 0; i < 8; i++)      
    free(board[i]);
  free(board);
}

void copie(piece *res, piece *ancien){
  res->cr1 = ancien->cr1;
  res->cr2 = ancien->cr2;
  res->cr3 = ancien->cr3;
  res->col = ancien->col;
}

int nbcarre(piece t){
  int res;

  res = 0;
  if (t.cr1 == 2)
    res++;
  if (t.cr2 == 2)
    res++;
  if (t.cr3 == 2)
    res++;
  return res;
}

int nbrond(piece t){
  int res;

  res = 0;
  if (t.cr1 == 1)
    res++;
  if (t.cr2 == 1)
    res++;
  if (t.cr3 == 1)
    res++;
  return res;
}

int is_correct_col(char c){
  if (c < 'a' || c > 'h')
    return -1;
  return 0;
}

int is_correct_line(char c){
  if (c < '1' || c > '8')
    return -1;
  return 0;
}

void removeCarre(piece *piece){
  if (piece->cr3 == 2)
    piece->cr3 = 0;
  else if (piece->cr2 == 2)
    piece->cr2 = 0;
  else
    piece->cr1 = 0;
}

void removeRond(piece *piece){
  if (piece->cr3 == 1)
    piece->cr3 = 0;
  else if (piece->cr2 == 1)
    piece->cr2 = 0;
  else
    piece->cr1 = 0;
}

void addCarre(piece *piece){
  if(piece->cr1 == 0)
    piece->cr1 = 2;
  else if(piece->cr2 == 0)
    piece->cr2 = 2;
  else
    piece->cr3 = 2;
}

void addRond(piece *piece){
  if(piece->cr1 == 0)
    piece->cr1 = 1;
  else if(piece->cr2 == 0)
    piece->cr2 = 1;
  else
    piece->cr3 = 1;
}

piece** initialise(){
  int i;
  int j;
  piece** res;

  res = (piece **)(malloc(sizeof(piece *) * 8));
  for (i = 0; i < 8; i++)
    res[i] = (piece *)(calloc(8, sizeof(piece)));
  for (j = 0; j < 8; j++){
    res[0][j].cr1 = j % 2 + 1;
    res[1][j].cr1 = -(j % 2) + 2;
    res[0][j].col = 'N';
    res[1][j].col = 'N';

    res[6][j].cr1 = j % 2 + 1;
    res[7][j].cr1 = -(j % 2) + 2;
    res[6][j].col = 'B';
    res[7][j].col = 'B';
  }
  for (i = 2; i < 6; i++)
    for (j = 0; j < 8; j++)
      res[i][j].col = '0';
  return res;
}

piece** ficinitialise(char *fichier, int *start){
  int i, j;
  FILE *f;
  char *tmp;
  piece** res;

  res = (piece **)(malloc(sizeof(piece *) * 8));
  for (i = 0; i < 8; i++)
    res[i] = (piece *)(calloc(8, sizeof(piece)));

  f = fopen(fichier,"r");
  if (f == NULL){
    freeall(res);
    return NULL;
  }

  i = 7;
  j = 0;
  tmp = (char *)(malloc(3 * sizeof(char)));
  while (fgets(tmp, 4, f)){
    if (strcmp(tmp, "N") == 0){
      *start = 1;
      break;
    }
    if (i < 0){
      freeall(res);
      free(tmp);
      fclose(f);
      return NULL;
    }

    if (tmp[0] == 'V')
      res[i][j].col = '0';
    else
      res[i][j].col = tmp[0];
    if ((tmp[0] != 'V' && tmp[0] != 'B' && tmp[0] != 'N') || tmp[1] > '9' || tmp[1] < '0'){
      freeall(res);
      fclose(f);
      free(tmp);
      return NULL;
    }
    switch(tmp[1]){
    case '0' : 
      res[i][j].cr1 = 0; res[i][j].cr2 = 0; res[i][j].cr3 = 0;
      break;
    case '1' : 
      res[i][j].cr1 = 2; res[i][j].cr2 = 0; res[i][j].cr3 = 0;
      break;
    case '2' : 
      res[i][j].cr1 = 1; res[i][j].cr2 = 0; res[i][j].cr3 = 0;
      break;
    case '3' : 
      res[i][j].cr1 = 2; res[i][j].cr2 = 2; res[i][j].cr3 = 0;
      break;
    case '4' : 
      res[i][j].cr1 = 1; res[i][j].cr2 = 1; res[i][j].cr3 = 0;
      break;
    case '5' : 
      res[i][j].cr1 = 2; res[i][j].cr2 = 2; res[i][j].cr3 = 2;
      break;
    case '6' : 
      res[i][j].cr1 = 1; res[i][j].cr2 = 1; res[i][j].cr3 = 1;
      break;
    case '7' : 
      res[i][j].cr1 = 2; res[i][j].cr2 = 1; res[i][j].cr3 = 0;
      break;
    case '8' : 
      res[i][j].cr1 = 2; res[i][j].cr2 = 2; res[i][j].cr3 = 1;
      break;
    case '9' : 
      res[i][j].cr1 = 2; res[i][j].cr2 = 1; res[i][j].cr3 = 1;
      break;
    }
    if (j == 7){
      i--;
      j = 0;
    }
    else
      j++;
  }
  free(tmp);
  fclose(f);
  if (i != -1 || j!= 0){
    freeall(res);
    return NULL;
  }
  return res;
}

void ficwrite(piece** tab, char* name, int start){
  FILE *f;
  piece toto;
  int i, j;

  f = fopen(name, "w+");
  for (i = 7; i >= 0; i--)
    for (j = 0; j < 8; j++){
      toto = tab[i][j];
      if (toto.col == '0'){
	fputc('V',f);
	fputc('0',f);
	fputc('\n',f);
      }
      else{
	if (toto.col == 'B')
	  fputc('B', f);
	else
	  fputc('N', f);
	switch(nbcarre(toto)){
	case 0 :
	  switch(nbrond(toto)){
	  case 1 : 
	    fputc('2',f);
	    break;
	  case 2 : 
	    fputc('4',f);
	    break;
	  default :
	    fputc('6',f);
	  }
	  break;
	case 1 : 
	  switch(nbrond(toto)){
	  case 0 :
	    fputc('1',f);
	    break;
	  case 1 :
	    fputc('7',f);
	    break;
	  default :
	    fputc('9',f);
	  }
	  break;
	case 2 :
	  if (nbrond(toto) == 0)
	    fputc('3',f);
	  else
	    fputc('8',f);
	  break;
	default :
	  fputc('5',f);
	}
	if (i != 0 || j != 7)
	  fputc('\n', f);
      }
    }
  if (start == 1){
    fputc('\n', f);
    fputc('N', f);
  }
  fclose(f);
}
	

	
