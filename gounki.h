#ifndef _GOUNKI_H_
#define _GOUNKI_H_

typedef struct piece{
  int cr1;
  int cr2;
  int cr3;
  char col;
} piece;

void copie(piece *res, piece *ancien);
int nbcarre(piece t);
int nbrond(piece t);
int is_correct_line(char c);
int is_correct_col(char c);
void removeCarre(piece *piece);
void removeRond(piece *piece);
void addCarre(piece *piece);
void addRond(piece *piece);
piece** initialise();
void printtab(piece** tab);
int abs(int x);
piece** copieboard(piece **ancien);
void freeall(piece** board);
piece** ficinitialise(char *fichier, int* start);
void ficwrite(piece** tab, char* name, int start);

int deploiement(piece **board, char *input, char playercol, char changeval);

int mouvement(piece **board, char *input, char playercol, char changeval);

int victoire(piece** board, char* input, char playercol);
int elim(piece** board, char playercol);

char* genrandom(piece** board, char botcolor);
#endif
